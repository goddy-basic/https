package com.goddy.httpsbasic.common.config;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Goddy
 * @date Create in 下午9:28 2018/6/13
 * @description
 */
@Configuration
public class httpConfig {

    @Value("${http.port}")
    private Integer port;

    @Value("${server.port}")
    private Integer redirectPort;

    @Value("${http.protocol}")
    private String protocol;

    @Bean
    public ServletWebServerFactory serverFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint securityConstraint = new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
            }
        };
        factory.addAdditionalTomcatConnectors(createSslConnector());
        return factory;
    }

    private Connector createSslConnector() {
        Connector connector = new Connector(protocol);
        connector.setPort(port);
        /*
         * true重新导向
         * false不重新导向
         */
        connector.setSecure(false);
        connector.setRedirectPort(redirectPort);
        return connector;
    }
}
