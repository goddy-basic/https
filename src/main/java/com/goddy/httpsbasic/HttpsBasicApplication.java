package com.goddy.httpsbasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HttpsBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(HttpsBasicApplication.class, args);
    }
}
